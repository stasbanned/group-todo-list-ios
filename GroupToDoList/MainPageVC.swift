//
//  MainVC.swift
//  GroupToDoList
//
//  Created by Станислав Тищенко on 22.09.2018.
//  Copyright © 2018 Станислав Тищенко. All rights reserved.
//

import UIKit
import Firebase

class MainPageVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    var loginViewCoordinateX: CGFloat?
    var fromImagePicker = false

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    @IBOutlet weak var signupLoginView: UIView!
    @IBOutlet var emailAndPassView: UIView!
    @IBOutlet var backDoneButtonsView: UIView!
    @IBOutlet var nameAndPhoneView: UIView!
    
    @IBOutlet weak var doneOrNextButtonOutlet: UIButton!
    @IBOutlet weak var avatarButtonOutlet: UIButton!
    
    @IBAction func signupButtonAction(_ sender: Any) {
        UIView.animate(withDuration: 1, animations: {
            self.behindScreen(view: self.signupLoginView)
            self.replaceOriginX(in: self.emailAndPassView)
            self.replaceOriginX(in: self.backDoneButtonsView)
            self.doneOrNextButtonOutlet.setTitle("Next", for: .normal)
        }, completion: nil)
    }

    @IBAction func loginButtonView(_ sender: Any) {
        UIView.animate(withDuration: 1, animations: {
            self.behindScreen(view: self.signupLoginView)
            self.replaceOriginX(in: self.emailAndPassView)
            self.replaceOriginX(in: self.backDoneButtonsView)
            self.doneOrNextButtonOutlet.setTitle("Done", for: .normal)
        }, completion: nil)
    }

    @IBAction func backButtonAction(_ sender: Any) {
        UIView.animate(withDuration: 1, animations: {
            if self.emailAndPassView.frame.origin.x == self.loginViewCoordinateX! {
                self.replaceOriginX(in: self.signupLoginView)
                self.overScreen(view: self.emailAndPassView)
                self.overScreen(view: self.backDoneButtonsView)
            }
            if self.nameAndPhoneView.frame.origin.x == self.loginViewCoordinateX! {
                self.replaceOriginX(in: self.emailAndPassView)
                self.overScreen(view: self.nameAndPhoneView)
                self.doneOrNextButtonOutlet.setTitle("Next", for: .normal)
            }
        }, completion: nil)
    }
    
    @IBAction func doneOrNextButtonAction(_ sender: Any) {
        UIView.animate(withDuration: 1, animations: {
            if self.doneOrNextButtonOutlet.titleLabel?.text == "Next" {
                self.doneOrNextButtonOutlet.setTitle("Done", for: .normal)
                self.replaceOriginX(in: self.nameAndPhoneView)
                self.behindScreen(view: self.emailAndPassView)
            }
            if self.nameAndPhoneView.frame.origin.x == self.loginViewCoordinateX! {
                Auth.auth().createUser(withEmail: self.emailTextField.text!,
                                       password: self.passTextField.text!)
            }
        }, completion: nil)
        if nameAndPhoneView.frame.origin.x == loginViewCoordinateX! &&
            doneOrNextButtonOutlet.titleLabel?.text == "Done" {
            performSegue(withIdentifier: "AfterSignup", sender: self)
        }
    }
    @IBAction func avatarButtonAction(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        picker.allowsEditing = false
        self.present(picker, animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            avatarButtonOutlet.setImage(image, for: .normal)
            fromImagePicker = true
        }
        self.dismiss(animated: true, completion: nil)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.insertSubview(signupLoginView, at: 1)
        view.insertSubview(emailAndPassView, at: 1)
        view.insertSubview(backDoneButtonsView, at: 1)
        view.insertSubview(nameAndPhoneView, at: 1)
        passTextField.isSecureTextEntry = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !fromImagePicker {
            signupLoginView.frame.origin.x = UIScreen.main.bounds.width / 2 - signupLoginView.frame.width / 2
            signupLoginView.frame.origin.y = UIScreen.main.bounds.height / 2 - signupLoginView.frame.height / 2

            loginViewCoordinateX = signupLoginView.frame.minX
            overScreen(view: emailAndPassView)
            replaceOriginY(in: emailAndPassView, by: signupLoginView.frame.origin.y)

            overScreen(view: backDoneButtonsView)
            replaceOriginY(in: backDoneButtonsView, by: emailAndPassView.frame.maxY)

            overScreen(view: nameAndPhoneView)
            replaceOriginY(in: nameAndPhoneView, by: signupLoginView.frame.origin.y)

            addLeftImageTo(textField: emailTextField, img: UIImage(named: "email")!)
            addLeftImageTo(textField: passTextField, img: UIImage(named: "pass")!)
            addLeftImageTo(textField: nameTextField, img: UIImage(named: "user")!)
            addLeftImageTo(textField: phoneTextField, img: UIImage(named: "phone")!)
        }
        fromImagePicker = false
    }

    func addLeftImageTo(textField: UITextField, img: UIImage) {
        let leftImageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: img.size.width, height: img.size.height))
        leftImageView.image = img
        textField.leftView = leftImageView
        textField.leftViewMode = .always
    }

    func replaceOriginX(in view: UIView) {
        view.frame.origin.x = loginViewCoordinateX!
    }

    func replaceOriginY(in view: UIView, by y: CGFloat) {
        view.frame.origin.y = y
    }

    func overScreen(view: UIView) {
        view.frame.origin.x = UIScreen.main.bounds.width
    }
    func behindScreen(view: UIView) {
        view.frame.origin.x = -view.frame.width
    }
}
