//
//  FirstRunAppVC.swift
//  GroupToDoList
//
//  Created by Станислав Тищенко on 23.09.2018.
//  Copyright © 2018 Станислав Тищенко. All rights reserved.
//

import UIKit
import UserNotifications

class FirstRunAppVC: UIViewController, UIScrollViewDelegate {
    let widthOfScreen = UIScreen.main.bounds.width
    let heightOfScreen = UIScreen.main.bounds.height

    @IBOutlet weak var slideScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (didAllow, error) in
            
        }

        slideScrollView.delegate = self
        let slides: [SlideView] = createSlides()
        setupSlideScrollView(slides: slides)
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubview(toFront: pageControl)
    }

    func createSlides() -> [SlideView] {
        let slide1 = FactoryOfSlides().createNewSlide(textOfLabel: "Slide1")
        let slide2 = FactoryOfSlides().createNewSlide(textOfLabel: "Slide2")

        return [slide1, slide2]
    }

    func setupSlideScrollView(slides: [SlideView]) {
        slideScrollView.frame = CGRect(x: 0.0,
                                       y: 0.0,
                                       width: widthOfScreen,
                                       height: heightOfScreen)
        slideScrollView.contentSize = CGSize(width: widthOfScreen * CGFloat(slides.count),
                                             height: heightOfScreen)

        slideScrollView.isPagingEnabled = true

        for i in 0..<slides.count {
            slides[i].frame = CGRect(x: widthOfScreen * CGFloat(i),
                                     y: 0.0,
                                     width: widthOfScreen,
                                     height: heightOfScreen)
            slideScrollView.addSubview(slides[i])
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/widthOfScreen)
        pageControl.currentPage = Int(pageIndex)
    }
}

class FactoryOfSlides: NSObject {
    func createNewSlide(textOfLabel: String) -> SlideView {
        let slide: SlideView = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! SlideView
        slide.label.text = textOfLabel
        return slide
    }
}
